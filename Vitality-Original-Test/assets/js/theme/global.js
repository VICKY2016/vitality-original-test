// import $ from 'jquery';
import 'history.js/scripts/bundled-uncompressed/html4+html5/jquery.history';
import PageManager                  from '../page-manager';
import quickSearch                  from './global/quick-search';
import currencySelector             from './global/currency-selector';
import menu                         from './global/menu';
import quickView                    from './global/quick-view';
import cartPreview                  from './global/cart-preview';
import compareProducts              from './global/compare-products';
import privacyCookieNotification    from './global/cookieNotification';
import maintenanceMode              from './global/maintenanceMode';
import carousel                     from './common/carousel';
import loadingProgressBar           from './global/loading-progress-bar';
import FastClick                    from 'fastclick';
import quickCart                    from './custom/quick-cart';
import flipclock                    from './custom/flipclock';
import youtubeModal                 from './custom/youtube-modal';
import wtbMap                       from './custom/wtb-map';
import cartMessages                 from './custom/cart-messages';

function fastClick(element) {
  return new FastClick(element);
}

export default class Global extends PageManager {
  /**
   * You can wrap the execution in this method with an asynchronous function map using the async library
   * if your global modules need async callback handling.
   * @param next
   */
  loaded(next) {
    fastClick(document.body);
    quickSearch();
    quickCart();
    currencySelector();
    quickView(this.context);
    cartPreview();
    cartMessages();
    compareProducts(this.context.urls);
    carousel();
    menu();
    window.MDSnackbars.init();
    window.snackbar = window.MDSnackbars;
    wtbMap();
    youtubeModal();
    flipclock();
    privacyCookieNotification();
    maintenanceMode(this.context.maintenanceMode);
    loadingProgressBar();
    next();
  }
}
