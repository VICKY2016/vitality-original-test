import $ from 'jquery';
import 'slick-carousel-browserify/slick/slick';
//import _ from 'lodash';
export default function () {
  // get every class with the class banner
  const $carousel = $('.banner');
  //attach this function to window, so when the youtube event fires, it can find it.

  //loop over all the divs with the classs name banner
  for (let i = 0; i < $carousel.length; i++) {
  //extract the inline options from them. Empty Object if there are none.

    //eslint-disable-next-line
    let options = $carousel.eq(i).attr('data-slick-options') || {},
    //eslint-disable-next-line
      extendedOptions,
    //eslint-disable-next-line
      $current;

    const defaultOptions = {
      prevArrow: '<button type="button" class="banner-prev"><i class="fa fa-angle-left fa-3x" aria-hidden="true"></i></button>',
      nextArrow: '<button type="button" class="banner-next"><i class="fa fa-angle-right fa-3x" aria-hidden="true"></i></button>',
    };

    //try and parse the options. If if fails, just default to an empty object.
    try {
      options = JSON.parse(options);
    } catch (err) {
      options = {};
    }
    //eslint-disable-next-line
    extendedOptions = $.extend(options, defaultOptions);

    //eslint-disable-next-line
    $current = $carousel.eq(i).slick(extendedOptions);
  }
}
