import $ from 'jquery';

/*
 * Manage the behaviour of a menu
 * @param {jQuery} $menu
 */
class Menu {
  constructor($menu) {
    const self                = this;
    this.$menu = $menu;
    this.$mobileButton = $menu.find('.mobile-button');
    this.$mainMenuDiv = $menu.find('.main-nav-menu');
    this.$categoryButton = $menu.find('.category-link');
    this.$categoriesMenuDiv = $menu.find('.category-dropdown');
    this.$body = $('body');

    this.$mobileButton.on('click', () => {
      self.toggle();
    });

    this.$categoryButton.on('click', () => {
      self.toggleCategories();
      return false;
    });
  }

  /**
    * toggle the nav bar
    */
  toggle() {
    this.$mainMenuDiv.toggle();
  }

  toggleCategories() {
    this.$categoriesMenuDiv.toggle();
  }

  /**
    * Check if the menu is a mobile one.
    */
  isMobile() {

  }

  close() {
    this.$mainMenuDiv.hide();
  }

  open() {
    this.$mainMenuDiv.show();
  }
}

/*
 * Create a new Menu instance
 * @param {string} [selector]
 * @return {Menu}
 */
export default function menuFactory(selector = '.main-nav') {
  const $menu = $(selector).eq(0);
  const menu = new Menu($menu);
  return menu;
}
