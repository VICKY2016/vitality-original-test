import $ from 'jquery';
import utils from '@bigcommerce/stencil-utils';
import ProductDetails from '../common/product-details';
import { defaultModal } from './modal';

export default function (context) {
  const modal = defaultModal();

  $('.quickview').on('click', (event) => {
    modal.open();
    const productId = $(event.currentTarget).data('product-id');

    utils.api.product.getById(productId, { template: 'products/quick-view' }, (err, response) => {
      modal.updateContent(response);
      // console.log(response);
      modal.$content.find('.productView').addClass('productView--quickView');

      return new ProductDetails(modal.$content.find('.quickView'), context);
    });
    return false;
  });
}
