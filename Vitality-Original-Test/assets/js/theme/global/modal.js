/* eslint no-multi-spaces: 0 */
import $ from 'jquery';

const bodyActiveClass       = 'has-activeModal';
const loadingOverlayClass   = 'loadingOverlay';
const modalBodyClass        = 'modal-body';
const modalContentClass     = 'modal-contents';

const SizeClasses = {
  small:  'modal-sm',
  large:  'modal-lg',
  normal: '',
};

export const ModalEvents = {
  close:  'hide.bs.modal',
  closed: 'hidden.bs.modal',
  open:   'show.bs.modal',
  opened: 'shown.bs.modal',
  loaded: 'loaded.bs.modal',
};

function getSizeFromModal($modal) {
  // console.log('getting the size');
  if ($modal.hasClass(SizeClasses.small)) {
    return 'small';
  }

  if ($modal.hasClass(SizeClasses.large)) {
    return 'large';
  }

  return 'normal';
}

function getViewportHeight(multipler = 1) {
  const viewportHeight = $(window).height();

  return viewportHeight * multipler;
}


function wrapModalBody(content) {
  const $modalBody = $('<div>');

  $modalBody
    .addClass(modalBodyClass)
    .html(content);

  return $modalBody;
}

function restrainContentHeight($content) {
  const $body           = $(`.${modalBodyClass}`, $content);
  const bodyHeight      = $body.outerHeight();
  const contentHeight   = $content.outerHeight();
  const viewportHeight  = getViewportHeight(0.9);
  const maxHeight       = viewportHeight - (contentHeight - bodyHeight);

  $body.css('max-height', maxHeight);
}

function createModalContent($modal) {
  let $content = $(`.${modalContentClass}`, $modal);

  if ($content.length === 0) {
    const existingContent = $modal.children();

    $content = $('<div>')
      .addClass(modalContentClass)
      .append(existingContent)
      .appendTo($modal);
  }

  return $content;
}

function createLoadingOverlay($modal) {
  let $loadingOverlay = $modal.find(`.${loadingOverlayClass}`);

  // console.log('creating that modal overlay', $modal.attr('class'));
  const loadingContent = '<i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i>';

  if ($loadingOverlay.length === 0) {
    $loadingOverlay = $('<div>')
      .html(loadingContent)
      .appendTo($modal);
  }

  return $loadingOverlay;
}

/**
 * Require foundation.reveal
 * Decorate foundation.reveal with additional methods
 * @param {jQuery} $modal
 * @param {Object} [options]
 * @param {string} [options.size]
 */
export class Modal {
  constructor($modal, { size = null } = {}) {
    // console.log($modal.attr('class'));

    this.$modal       = $modal;
    this.$content     = createModalContent(this.$modal);
    this.$overlay     = createLoadingOverlay(this.$modal);
    this.defaultSize  = size || getSizeFromModal($modal);
    this.size         = this.defaultSize;
    this.pending      = false;

    this.onModalOpen    = this.onModalOpen.bind(this);
    this.onModalOpened  = this.onModalOpened.bind(this);
    this.onModalClose   = this.onModalClose.bind(this);
    this.onModalClosed  = this.onModalClosed.bind(this);

    this.bindEvents();
  }

  get pending() {
    return this._pending;
  }

  set pending(pending) {
    // console.log('pending', pending);
    this._pending = pending;

    if (pending) {
      this.$overlay.show();
    } else {
      this.$overlay.hide();
    }
  }

  get size() {
    return this._size;
  }

  set size(size) {
    this._size = size;

    this.$modal
    .removeClass(SizeClasses.small)
    .removeClass(SizeClasses.large)
    .addClass(SizeClasses[size] || '');
  }

  bindEvents() {
    this.$modal.on(ModalEvents.close, this.onModalClose);
    this.$modal.on(ModalEvents.closed, this.onModalClosed);
    this.$modal.on(ModalEvents.open, this.onModalOpen);
    this.$modal.on(ModalEvents.opened, this.onModalOpened);
  }

  unbindEvents() {
    this.$modal.off(ModalEvents.close, this.onModalClose);
    this.$modal.off(ModalEvents.closed, this.onModalClosed);
    this.$modal.off(ModalEvents.open, this.onModalOpen);
    this.$modal.off(ModalEvents.opened, this.onModalOpened);
  }

  open({ size, pending = true, clearContent = true } = {}) {
    this.pending = pending;
    // console.log('opening');
    if (size) {
      this.size = size;
    }

    if (clearContent) {
      this.clearContent();
    }

    this.$modal.modal('show');
  }

  close() {
    // console.log('closing');
    this.$modal.modal('hide');
  }


  toggle() {
    // console.log('toggling');
    this.$modal.modal('toggle');
  }

  handleUpdate() {
    // console.log('updateing');
    this.$modal.modal('handleUpdate');
  }

  updateContent(content, { wrap = false } = {}) {
    let $content = $(content);

    if (wrap) {
      $content = wrapModalBody(content);
    }

    this.pending = false;
    this.$content.html($content);

    restrainContentHeight(this.$content);
    // foundation(this.$content);
  }

  clearContent() {
    this.$content.html('');
  }

  onModalClose() {
    // console.log('modal closing');
    $('body').removeClass(bodyActiveClass);
  }

  onModalClosed() {
    // console.log('modal closed');
    this.size = this.defaultSize;
  }

  onModalOpen() {
    $('body').addClass(bodyActiveClass);
  }

  onModalOpened() {
    restrainContentHeight(this.$content);
  }
}

/**
 * Return an array of modals
 * @param {string} selector
 * @param {Object} [options]
 * @param {string} [options.size]
 * @returns {array}
 */
export default function modalFactory(selector = '.modalitup', options = {}) {
  const $modals = $(selector, options.$context);

  return $modals.map((index, element) => {
    const $modal      = $(element);
    const instanceKey = 'modal-instance';
    const cachedModal = $modal.data(instanceKey);

    if (cachedModal instanceof Modal) {
      return cachedModal;
    }

    const modal = new Modal($modal, options);

    $modal.data(instanceKey, modal);

    return modal;
  }).toArray();
}

/*
 * Return the default page modal
 */
export function defaultModal(options) {
  const modal = modalFactory('#modal', options)[0];

  return modal;
}
