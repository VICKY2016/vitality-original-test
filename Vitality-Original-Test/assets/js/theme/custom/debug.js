/**
  Some utilities helpful for debugging.

  TODO: find a way to prevent this, and any calls to it to not be included in
        the build.
*/

/**
  * inspiration came from Embers assert function.
  */
export function assert(msg, test) {
  const throwAssertion = !test;

  if (throwAssertion) {
    throw new Error(`Assertion Failed: ${msg}`);
  }
}

/**
  * simply checks to see if your hostname is localhost.
  */
export function isDevelopment() {

}
