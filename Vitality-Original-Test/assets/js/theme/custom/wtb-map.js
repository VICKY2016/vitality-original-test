import $                from 'jquery';
export default function () {
  $(document).ready(() => {
    const biofreezeLink = document.createElement('a');
    biofreezeLink.setAttribute('href', 'http://wtb.biofreeze.com/WhereToBuy.aspx');
    biofreezeLink.setAttribute('target', '_blank');
    const biofreezeImg = document.createElement('img');
    biofreezeImg.setAttribute('src', 'https://vitalitydepot.ca/content/img/map-biofreeze.svg');
    const parentDiv = document.getElementById('feature-selector-panel');
    biofreezeLink.appendChild(biofreezeImg);
    parentDiv.appendChild(biofreezeLink);
  });
}
