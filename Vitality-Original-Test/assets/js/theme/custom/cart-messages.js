import $                from 'jquery';

export default function () {
  $(document).ready(() => {
    $('#cart-messages-modal').modal('show');
  });
}
