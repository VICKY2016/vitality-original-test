/* eslint no-console: 0*/
/* eslint no-multi-spaces: 0 */


import $                from 'jquery';
import { api }          from '@bigcommerce/stencil-utils';
import { defaultModal } from '../global/modal';
import notify           from './notify';
import qc               from './wishlist-util';
//import { assert }       from './debug';
import utils            from '@bigcommerce/stencil-utils';

/**
  * Returns the Promise with modal.
  */
function _spawnModal(template, { size = 'small' } = {}) {
  //assert('should pass in a template', template);

  const modal   = defaultModal({ size });
  const options = { template };

  modal.open();

  return new Promise((resolve, reject) => {
    api.getPage('', options, (err, content) => {
      if (err) {
        reject(modal.updateContent(err));
      }

      modal.updateContent(content, { wrap: true });
      resolve(modal);
    });
  });
}

function spawnWishlistEditModal(wishlistid) {
  _spawnModal('custom/edit-wishlist').then(modal => {
    $('#wishlist-edit-form').on('submit', evt => {
      const wishlistName = $('#wishlistname').val();

      evt.preventDefault();

      qc.rename(wishlistid, wishlistName).then(() => {
        notify.info(`successfully renamed Quick Cart to ${wishlistName}`);
        modal.close();
        $(`#${wishlistid}`).text(wishlistName);
      }).catch(error => {
        console.error(error);
        notify.info('Unexpected error, Quick Cart not renamed.');
        modal.close();
      }); // end qc create
    });
  });
}

function spawnWishlistCreateModal() {
  _spawnModal('account/add-wishlist').then(modal => {
    $('#wishlist-create-form').on('submit', evt => {
      const wishlistName = $('#wishlistname').val();

      evt.preventDefault();

      qc.create(wishlistName).then(() => {
        notify.info(`successfully created Wishlist ${wishlistName}`);
        modal.close();
      }).catch(error => {
        console.error(error);
        notify.info('Unexpected error, Wishlist not created.');
        modal.close();
      }); // end qc create
    });
  });
}

function spawnQuickCartCreateModal() {
  _spawnModal('custom/add-quick-cart').then(modal => {
    $('#quick-cart-create-form').on('submit', evt => {
      const quickCartName = $('#quick-cart-name').val();

      evt.preventDefault();

      qc.create(quickCartName, true).then(() => {
        notify.info('successfully created QuickCart');
        modal.close();
      }).catch(error => {
        console.error(error);
        notify.info('Unexpected error, QuickCart not created.');
        modal.close();
      }); // end qc create
    });
  });
}

/**
  takes in product id.
  outputs the product options, which are generated & created
  in the custom/dummy-json handlebars template
*/
function getProductOptions(pid) {
  //assert('should have a product id', pid);

  return new Promise((resolve, reject) => {
    const options = {
      template: 'custom/quick-cart-options',
    };
    utils.api.product.getById(pid, options, (err, res) => {
      //let data;

      if (err) {
        reject(err);
      }
      resolve(res);
    });
  });
}

function addToCart(pid, optionsUrl, qty) {
  // assert('should have a product id', pid);
  // assert('should have a options Url', option);
  // assert('Quantity should be above 0', qty > 0);

  const url = `/cart.php?action=add&product_id=${pid}${optionsUrl}&qty[]=${qty}`;

  return new Promise((resolve, reject) => {
    $.ajax({
      url,
      type: 'POST',
      success: (response) => {
        resolve(response);
      },
      error: (jqXHR, text, error) => {
        reject(error);
      },
    });
  });
}
function updateQuickCartPreview() {
  $('.quick-cart-preview-loaded').addClass('hidden');
  $('.quick-cart-preview-loading').removeClass('hidden');
  function getCartInfo() {
    return new Promise((resolve, reject) => {
      const options = {
        template: 'custom/quick-cart-preview',
      };
      utils.api.getPage('/cart.php', options, (err, res) => {
        //let data;

        if (err) {
          reject(err);
        }
        resolve(res);
      });
    });
  }
  getCartInfo()
  .then(res => {
    $('.quick-cart-preview').html(res);
    $('.quick-cart-preview-loading').addClass('hidden');
    $('.quick-cart-preview-loaded').removeClass('hidden');
  });
}

export default function () {
  $('.quick-cart-select').on('click', evt => {
    const target = $(evt.currentTarget);
    const selected = target.attr('data-wishlist-name');

    $('#quick-cart-selected')
      .html(selected)
      .attr('data-wishlist-id', target.attr('data-wishlist-id'));

    evt.preventDefault();
  });

  $('.quick-cart-create').on('click', evt => {
    spawnQuickCartCreateModal();
    evt.preventDefault();
  });

  $('.wishlist-create').on('click', evt => {
    spawnWishlistCreateModal();
    evt.preventDefault();
  });

  $('#quick-cart-form').on('submit', evt => {
    evt.preventDefault();

    const productTitle  = $('#quick-cart-product-title').val();
    const quickCart     = $('#quick-cart-selected').html();
    const pid           = $('#add-to-quick-cart').attr('data-product-id');
    const qcid          = $('#quick-cart-selected').attr('data-wishlist-id');

    if (!quickCart) {
      notify.info('please select a QuickCart');
      return false;
    }

    //assert('should have a product id', pid);
    //assert('should have a quick cart id', qcid);

    notify.loading(true);

    qc.add(qcid, pid).then(() => {
      notify.loading(false);
      notify.info(`${productTitle} has been added to ${quickCart} quick cart`);
    }).catch(err => {
      console.error(err);
      notify.loading(false);
      notify.info(`Sorry, ${productTitle} has NOT been added to ${quickCart} quick cart`);
    });

    return false;
  });

  $(document).ready(() => {
    $('.quick-cart-product[data-product-has-options="true"]').each(function () {
      const pid = parseInt($(this).attr('data-product-id'), 10);

      notify.loading(true, 'loading product options...');
      getProductOptions(pid).then(response => {
        $(this).find('.quick-cart-options').append(response);
        notify.loading(false);
      }).catch(err => {
        notify.info('failed to get product options');
        console.error(err);
      });
    }); //end each
    updateQuickCartPreview();

    $('.quick-cart-add-to-cart').on('submit', (evt) => {
      evt.preventDefault();

      const pid = $(evt.currentTarget).attr('id');
      const formData = $(evt.currentTarget).serializeArray();
      let quant = 0;
      let optionsUrl = '';
      if (formData.length > 1) {
        for (let i = 0; i < formData.length; i++) {
          if (formData[i].name === 'quantity') {
            quant = formData[i].value;
          } else {
            optionsUrl += `&attribute[${formData[i].name}]=${formData[i].value}`;
          }
        }
      } else {
        quant = formData[0].value;
      }

      //eslint-disable-next-line
      if (quant != 0) {
        notify.info(`Adding ${quant} items to the cart`);
        addToCart(pid, optionsUrl, quant).then(() => {
          notify.info(`${quant} items successfully added to cart.`);
          updateQuickCartPreview();
        }, (error) => {
          notify.info(`Failed to add ${quant} items to cart: ${error}`);
        });
      } else {
        notify.info('You must enter a valid quantity.');
      }
    });
    $('.quick-cart-help').popover();
    $(() => {
      $('[data-toggle="tooltip"]').tooltip();
    });
    $('.quick-cart-edit').on('click', (evt) => {
      const wishlistid = (evt.target.id);
      spawnWishlistEditModal(wishlistid);
    });
  });
}
