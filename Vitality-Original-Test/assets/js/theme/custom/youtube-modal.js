import $                from 'jquery';

$('#ergoback').on('hidden.bs.modal', () => {
  $('#ergoback iframe').attr('src', '');
});
$('#ergoback').on('shown.bs.modal', () => {
  $('#ergoback iframe').attr('src', 'https://www.youtube.com/embed/ci_L-vdNBzs');
});

$('#ezmend').on('hidden.bs.modal', () => {
  $('#ezmend iframe').attr('src', '');
});
$('#ezmend').on('shown.bs.modal', () => {
  $('#ezmend iframe').attr('src', 'https://www.youtube.com/embed/a1RN-LZzkK4');
});

$('#paingone').on('hidden.bs.modal', () => {
  $('#paingone iframe').attr('src', '');
});
$('#paingone').on('shown.bs.modal', () => {
  $('#paingone iframe').attr('src', 'https://www.youtube.com/embed/SrMZA16IBwE');
});

$('#posturemedic').on('hidden.bs.modal', () => {
  $('#posturemedic iframe').attr('src', '');
});
$('#posturemedic').on('shown.bs.modal', () => {
  $('#posturemedic iframe').attr('src', 'https://www.youtube.com/embed/RrrXaeaX6fA');
});
