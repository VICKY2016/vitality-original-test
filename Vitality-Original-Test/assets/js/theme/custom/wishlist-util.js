/**
  Because quick carts are wishlists, this is actually good for both.
*/

import $ from 'jquery';

const endpoint  = '/wishlist.php';

/**
  * puts the product in the given quick cart.
  * essentially the url looks like this:
  * http://127.0.0.1:3000/wishlist.php?action=add&product_id=344
  * @param qcName
  * @param pid
  */
function add(wishlistid, pid) {
  const method    = 'GET';
  const data      = {
    action      :'add',
    wishlistid,
    product_id  : pid,
  };

  return new Promise((resolve, reject) => {
    $.ajax({
      url: endpoint,
      method,
      data,
    }).done(reponse => {
      resolve(reponse);
    }).fail(err => {
      reject(err);
    });
  }); //end promise
}

/**
  * get all the products for a given wishlist.
  */
function get(wishlistid) {
  return wishlistid;
}

/**
  * rename a wishlist with the given id.
  */
function rename(wishlistid, name) {
  const method = 'POST';
  const url = `${endpoint}?action=editwishlist`;
  const data = {
    submit: null,
    wishlistname: name,
    publicwishlist: 'on',
    wishlistid,
  };
  return new Promise((resolve, reject) => {
    $.ajax({ url, method, data })
      .done(response => {
        resolve(response);
      })
      .fail(err => {
        reject(err);
      });
  });
}

/**
  * creates a new wishlist.
  */
function create(wishlistName, isPublic = false) {
  const method    = 'POST';
  const url = `${endpoint}?action=addwishlist`;
  const data = {
    submit          : null,
    wishlistname    : wishlistName,
  };

  if (isPublic) {
    data.publicwishlist = 'on';
  }

  return new Promise((resolve, reject) => {
    $.ajax({
      url,
      method,
      data,
    }).done(response => {
      resolve(response);
    }).fail(err => {
      reject(err);
    });
  }); //end promise
}

export default { add, create, get, rename };

