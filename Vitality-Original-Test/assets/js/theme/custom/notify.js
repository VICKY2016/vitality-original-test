/* global MDSnackbars, History */

/**
  * displays an indicator to the user that something is loading.
  */
function loading(val, msg = 'loading...') {
  const text = `<i class="fa fa-spin fa-circle-o-notch" aria-hidden="true"></i> ${msg}`;
  const options = {
    text,
    timeout: 0,
    clickToClose: false,
    html: true,
    animation: 'fade',
  };

  if (val) {
    MDSnackbars.show(options);
  } else {
    MDSnackbars.hide();
  }
}

/**
  * displays the snackbar, with the given msg.
  */
function info(msg) {
  const options = {
    text: msg,              // change snackbar's text/html
    clickToClose: true,     // enable/disable the click to close behavior
    animation: 'fade',      // change the animation type ('fade' or 'slideup', default to 'fade')
  };
  MDSnackbars.show(options);
}

const notify = {
  info,
  loading,
};

export default notify;
