/*
 Import all product specific js
 */
import $ from 'jquery';
import PageManager from '../page-manager';
//import Review from './product/reviews';
import collapsibleFactory from './common/collapsible';
import ProductDetails from './common/product-details';
import videoGallery from './product/video-gallery';
import thumbnailResize from './product/thumbnail-resize';
//import { classifyForm } from './common/form-utils';
import { defaultModal } from './global/modal';

// <a href="#" class="btn btn-default btn-sm card-figcaption-button quickview" data-product-id="{{id}}">
                            // <i class="fa fa-bolt" aria-hidden="true"></i>
                            // {{lang 'products.quick_view'}}
                        // </a>
thumbnailResize();
const History = window.History;
export default class Product extends PageManager {
  constructor() {
    super();
    this.url = location.href;
    this.$reviewLink = $('[data-reveal-id="modal-review-form"]');
  }

  before(next) {
    // Listen for foundation modal close events to sanitize URL after review.
    $(document).on('close.fndtn.reveal', () => {
      if (this.url.indexOf('#writeReview') !== -1) {
        History.replaceState('', document.title, window.location.pathname);
      }
    });

    next();
  }

  loaded(next) {
    //let validator;

    // Init collapsible
    collapsibleFactory();

    this.productDetails = new ProductDetails($('.productView'), this.context);

    videoGallery();

    // const $reviewForm = classifyForm('.writeReview-form');
    // const review = new Review($reviewForm);

    // $('[data-reveal-id="modal-review-form"]').on('click', () => {
    //   validator = review.registerValidation();
    // });

    const modal = defaultModal();

    $('[data-reveal-id="modal-review-form"]').on('click', () => {
      modal.open();
      modal.updateContent($('#modal-review-form').html());
      return false;
    });

    // $reviewForm.on('submit', () => {
    //   if (validator) {
    //     validator.performCheck();
    //     return validator.areAll('valid');
    //   }

    //   return false;
    // });

    next();
  }

  after(next) {
    this.productReviewHandler();

    next();
  }

  productReviewHandler() {
    if (this.url.indexOf('#writeReview') !== -1) {
      this.$reviewLink.click();
    }
  }
}
