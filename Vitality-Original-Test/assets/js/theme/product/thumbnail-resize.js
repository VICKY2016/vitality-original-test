import $ from 'jquery';

export default function () {
  $('.productView-thumbnail a img').each(function () {
    const maxWidth = 250; // Max width for the image
    const maxHeight = 250; // Max height for the image
    let ratio = 0;  // Used for aspect ratio
    let width = $(this).width();    // Current image width
    let height = $(this).height();  // Current image height

    // Check if the current width is larger than the max
    if (width > maxWidth) {
      ratio = maxWidth / width;   // get ratio for scaling image
      $(this).css('width', maxWidth); // Set new width
      $(this).css('height', height * ratio);  // Scale height based on ratio
      height = height * ratio;    // Reset height to match scaled image
      width = width * ratio;    // Reset width to match scaled image
    }

    // Check if current height is larger than max
    if (height > maxHeight) {
      ratio = maxHeight / height; // get ratio for scaling image
      $(this).css('height', maxHeight);   // Set new height
      $(this).css('width', width * ratio);    // Scale width based on ratio
      width = width * ratio;    // Reset width to match scaled image
    }
  });
}
