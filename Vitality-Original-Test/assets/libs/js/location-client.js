'use strict';
//https://googlemaps.github.io/js-store-locator/examples/custom.html

/**
 * @extends storeLocator.StaticDataFeed
 * @constructor
 */
function VitalityDepotDataSource(options) {
  $.extend(this, new storeLocator.StaticDataFeed);

  this.options = options;
  var queryString = '';
  if (this.product) {
    queryString
  }

  var that = this;

  $.getJSON('//posturemedic.com/api/public/v1/stores'+queryString).done(function(data) {
    that.setStores(that.parse_(data));
  }).fail(function() {
    console.warn("unable to fetch data from server.");
  });

}

/**
 * @const
 * @type {!storeLocator.FeatureSet}
 * @private
 */
VitalityDepotDataSource.prototype.FEATURES_ = new storeLocator.FeatureSet(
  new storeLocator.Feature('product-1', 'ErgoBack', {
    img: 'https://vitalitydepot.ca/content/img/map-ergoback.svg',
    prefferedProperty: 'img'
  }),
  new storeLocator.Feature('product-2', 'PostureMedic', {
    img: 'https://vitalitydepot.ca/content/img/map-posturemedic.svg',
    prefferedProperty: 'img'
  }),
  new storeLocator.Feature('product-3', 'PainGone', {
    img: 'https://vitalitydepot.ca/content/img/map-paingone.svg',
    prefferedProperty: 'img'
  }),
  new storeLocator.Feature('product-4', 'EZMend', {
    img: 'https://vitalitydepot.ca/content/img/map-ezmend.svg',
    prefferedProperty: 'img'
  })
);

/**
 * @return {!storeLocator.FeatureSet}
 */
VitalityDepotDataSource.prototype.getFeatures = function() {
  return this.FEATURES_;
};

/**
  * Make table out of a two dimensional array. But in a string.
  * Now i know what you're thinking - why not make it a DOM Object?
  * Good question. It's cause we're reusing code provided by google, and it
  * doesnt handle proper outputting of a DOM Element. it just sayins [Object blahblah]
  * If you can find a solution to do this, bless your little soul.
  * @return String tableString
  */
VitalityDepotDataSource.prototype.createTable = function(tableData) {
  var table = '<table><tbody>',
      row, cell;

  tableData.forEach(function(rowData) {
    row = '<tr>';
    rowData.forEach(function(cellData) {
      cell = '<td>' + cellData + '</td>';
      row  = row + cell;
    });
    row = row + '</tr>'

    table = table + row;
  });

  table = table + '</tbody></table>';
  return table;
}

/**
 * Generates a HTML list of this store's features.
 * @private
 * @return {string} html list of this store's features.
 */
VitalityDepotDataSource.prototype.generateFeaturesHTML_ = function() {

};

/**
  * Takes in the data of a store, and extracts out the hours converting it into
  * a two dimensional array. If a store has no hours (is closed every day, we'll
  * just not output anything.
  *
  */
VitalityDepotDataSource.prototype.generateHoursTable = function(store) {
  var days        = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'],
      rows        = [],
      hasOpenDay  = false;

  for (var d = 0; d < days.length ; d++) {
    var day   = days[d],
        open  = store[days[d] + '_open'],
        close = store[days[d] + '_close'];

    //console.log(open !== "00:00:00");
    if (open && open !== null && open !== "00:00:00") {
      rows.push([
        day,
        moment(open, 'HH:mm:SS').format('h:mm a'),
        moment(close, 'HH:mm:SS').format('h:mm a')
      ]);
      hasOpenDay = true;
    } else {
      rows.push([ day, 'closed', '']);
    }

    //check if the store had any open hours.
  }

  if (!hasOpenDay) {
    return null;
  }

  return this.createTable(rows);
};

/**
 * @private
 * @param {string} csv
 * @return {!Array.<!storeLocator.Store>}
 */
VitalityDepotDataSource.prototype.parse_ = function(data) {
  var stores = [];

  for (var i = 0; i < data.length ; i++) {
    var store     = data[i],
        products  = store.products || [],
        features  = new storeLocator.FeatureSet,
        address2  = store.city + ', ' + store.province_state + ', ' + store.country + ', ' + store.postal_zip,
        product, position;

    //parse the features ('products') of the store.
    for (var p = 0; p < products.length ; p++) {
      product = this.FEATURES_.getById('product-' + products[p]);
      features.add(product);
    }

    //init a new lng/lat point
    position = new google.maps.LatLng(store.latitude, store.longitude);

    var builtStore = new storeLocator.Store(store.id, position, features, {
      title   : store.name,
      address : store.address + ' ' + address2,
      hours   : this.generateHoursTable(store),
      misc    : store.notes,
      website : store.website,
      email   : store.email,
      phone   : store.phone
    }, {
      fields: ["title","address","hours","phone","misc"]
    });

/*
    builtStore.generateFeaturesHTML_ = function() {
      var html = [];
      html.push('<ul class="features">');
      var featureList = this.features_.asList();
      for (var i = 0, feature; feature = featureList[i]; i++) {
        html.push('<li>');
        html.push(feature.getDisplayName());
        //html.push(<img src="">feature.getDisplayName());
        html.push('</li>');
      }
      html.push('</ul>');
      return html.join('');
    };
*/


    // we're over riding the built in getInfoPanelContent to add more fields.
    builtStore.getInfoPanelContent = function() {
      var details = this.getDetails();
      return  '<div class="store"><div class="title">' +
              details.title +
              '</div><div class="address">' +
              details.address +
              '</div>' + '<div class="phone">' +
              details.phone;
    };

    stores.push(builtStore);
  }

  return stores;
};

google.maps.event.addDomListener(window, 'load', function() {
  var map = new google.maps.Map(document.getElementById('map-canvas'), {
    center    : new google.maps.LatLng(43.784772, -79.482216),
    zoom      : 13,
    mapTypeId : google.maps.MapTypeId.ROADMAP
  });

  //check if geolocation is enabled.
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
      map.setCenter(initialLocation);
    });
  } else {
    console.log("geolocation not supported, so we put you in the middle of nowhere.")
  }

  var panelDiv = document.getElementById('panel');
  var data = new VitalityDepotDataSource;
  var view = new storeLocator.View(map, data, {
    geolocation : false,
    features    : data.getFeatures()
  });

  view.createMarker = function(store) {
    var marker = new google.maps.Marker({
      position  : store.getLocation(),
      animation : google.maps.Animation.DROP,
      title     : store.getDetails().title,
    });

    return marker;
  };

  var panel = new storeLocator.Panel(panelDiv, {
    view: view,
    directions: false,
    featureFilter: false
  });


  var featureSelPanel = document.getElementById('feature-selector-panel');

  new storeLocator.FeatureSelectPanel(featureSelPanel, {
    panel: panel,
    view: view,
    onlyOneSelectable: true,
  });

  //pickle.toggleFeatureFilter_(data.FEATURES_.array_[1]);
      //that.get('view').refreshView(););
});
