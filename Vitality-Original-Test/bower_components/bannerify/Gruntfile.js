module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    copy: {
      css: {
        nonull: true,
        cwd: ".",
        flatten: true,
        src: [
          'bower_components/slick-carousel/slick/slick.css',
          'bower_components/slick-carousel/slick/slick-theme.css'
        ],
        dest: './dist/css',
        expand: true
      },

      sass: {
        nonull: true,
        cwd: ".",
        flatten: true,
        src: [
          'src/bannerify.scss'
        ],
        dest: './dist/scss',
        expand: true
      },

      js: {
        nonull: true,
        cwd: ".",
        flatten: true,
        src: [
          'src/bannerify.js',
          'bower_components/jquery-throttle-debounce/jquery.ba-throttle-debounce.min.js',
          'bower_components/slick-carousel/slick/slick.js',
        ],
        dest: './dist/js',
        expand: true
      },

      fonts: {
        nonull: true,
        cwd: ".",
        flatten: true,
        src: [
          'bower_components/slick-carousel/slick/fonts/*',
        ],
        dest: './dist/fonts',
        expand: true
      }
    }, //end copy

    concat: {
      css: {
        src: ['dist/css/bannerify.css', 'dist/css/slick-theme.css', 'dist/css/slick.css'],
        dest: 'dist/bannerify.css',
      },

      js: {
        src: ['dist/js/jquery.ba-throttle-debounce.min.js', 'dist/js/slick.js', 'dist/js/bannerify.js'],
        dest: 'dist/bannerify.js',
      },
    },

    clean: {
      dist: {
        options: {
          force: true //what the hell? we're not outside the cwd, why do we need to do this?
        },
        src: [
          'dist'
        ]
      },
      leftovers: {
        options: {
          force: true //what the hell? we're not outside the cwd, why do we need to do this?
        },
        src: [
          'dist/css',
          'dist/js'
        ]
      },
    },

    postcss: {
      options: {
        processors: [
          require('autoprefixer')({browsers: 'last 4 versions'}), // add vendor prefixes
        ]
      },
      dist: {
        src: 'dist/*.css'
      }
    }, //end postcss

    cssmin: {
      options: {

      },
      dist: {
        files: {
          'dist/bannerify.min.css': 'dist/bannerify.css'
        }
      }
    },

    uglify: {
      options: {
        preserveComments: 'some' //this doesn't do what it's supposed to, and it's annoying...
      },
      dist: {
        files: {
          'dist/bannerify.min.js': 'dist/bannerify.js'
        }
      }
    },

    watch: {
      scripts: {
        files: ['src/*'],
        tasks: ['build'],
        options: {
          spawn: false,
          livereload: true,
        },
      },
    },

    sass: {
      options: {
        sourceMap: true
      },
      dist: {
        files: {
          'dist/css/bannerify.css': 'src/bannerify.scss'
        }
      }
    },

/*
    jsdoc2md: {
      docs: {
          src: "js/*.js",
          dest: "api/api.md",
      },
    },
*/

  });

  // Load the plugin that provides the "uglify" task.
  require('load-grunt-tasks')(grunt);

  grunt.registerTask('build', [
    'clean:dist',
    'copy:css',
    'copy:sass',
    'copy:js',
    'copy:fonts',
    'sass',
    'concat:css',
    'concat:js',
    'cssmin:dist',
    'postcss:dist',
    'uglify:dist',
    'clean:leftovers'
  ]);

};