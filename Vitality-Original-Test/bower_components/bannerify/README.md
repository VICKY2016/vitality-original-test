#Basic usage:
Include both the js and css files in the `dist` directory.
```
function onYouTubeIframeAPIReady() {
  $('.banner').youtubeReady();
}

$('.banner').slick({
  touchMove     : false,
  autoplaySpeed : 5000,
  dots          : true,
  arrows        : false
}).bannerify({
  dotColor: "#ccc",
  dotHoverColor: "red",
  dotPositionClass: "top right"
});

$(window).resize( $.debounce( 250, false, function(evt){ 
  $('.banner').adjustVideoHeight();
}));

```
**onYouTubeIframeAPIReady() must be in global namespace.**

#Dependancies:

The only real dependency is `JQuery`. All the other dependencies `slick.js`, `jqeury-throttle-debouce`, and `youtubeIframeAPI` are all built and minified into the library.


#Features
  * fully responsive (adapts to 100% the width of its container)
    * scales videos nicely
    * finger swipe navigation.
    * can choose to remove dots
  * pauses the video when it's started, and automatically restarts if videos ends, or a is paused for *x* amount of time
  * pauses the video when next slide button is hit.
  * height is determined by largest image, videos will automatically scale to fit this.
  * easy to implement (simple jQuery Plugin), easy to extend.
  * fully customizable
    * colors
    * delays
    * navigation position.
  * you can have multiple on the same page.
  * can contain captions