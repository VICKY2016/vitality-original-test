# Stencil theme for vitalitydepot.ca

##**Installation**

Install general dependencies: `nodejs` `npm` `git`, install these with your package manager `brew, apt-get, pacman, yum, chocolatey`
example for ubuntu/debian:
```
sudo apt-get install nodejs npm git 
```
* * *
Install global node dependencies, with the -g flag
```
npm install -g jspm bower grunt-cli karma phantomjs-prebuilt jasmine
```
* * *
Install the stencil client
```
npm install -g bigcommerce/stencil-cli
```
* * *
Clone this git repo into your working directory

`git clone https://[username]@bitbucket.org/vitalitydepot/vitalitydepot-stencil.git` replace [username] with your name,

or

`git clone git@bitbucket.org:vitalitydepot/vitalitydepot-stencil.git` (if you have your ssh key set up with bitbucket, which you will need for the bower dependencies)
* * *
`cd` into the folder you just created and install local deps:
```
npm install
jspm install
bower install
```
note: you will need your ssh keys configured with github in order to run `bower install` successfully, as it pulls from a few of our own private repos
* * *
Enable the git hooks:
```
cp .hooks/pre-commit .git/hooks
```
* * *
start the stencil server:
```
stencil start
```
you will have to set up the .stencil file first, either use one of the ones provided, or run `stencil init` to create your own
* * *

##**Staging**
Staging is currently done on the Livita site
Unfortunately BC does not allow identical local/staging builds due to limitations like the account page, so we will have to do testing here.
To switch between the files

```cp .stencil.vd .stencil``` <- use the vd credentials
```cp .stencil.liv .stencil``` <- use the livita credentials
* * *

##**Contributing to this project:**

##### Git usage
Create your own feature branch, name it `yourname/feature`

e.g. `git checkout -b carlos/homepage`

Do all your work on this branch. When you are ready to merge your commits to master:
```
git checkout master
git pull origin master
git checkout <feature-branch>
git merge origin/master
```
This checks out the master branch, and gets the latest version from the bitbucket repository. Then you check back into your feature branch, and merge master into it.

When you have resolved any potential conflicts, 
```
git checkout master
git merge <feature-branch>
git push
```
This merges your branch back into master, at this point, you can now push your changes.

Note that once we have the site live for the first time, we will switch to a 'production' branch and a 'development' branch. Nobody should be editing the live site directly.
* * *
##### CSS
All styles should be in `/assets/scss/oursass/` as a .scss file and `@imported` in the `/assets/scss/theme.scss` file.

Try to define colours in `oursass/colours.scss` and `@import` that file to re-use colours throughout the site and keep it easy to maintain.
* * *
##### JS
All custom js should be in `/assets/js/custom/` and must pass the lint tests: [JS Style Guide](https://github.com/airbnb/javascript)

`import` your custom js file in `assets/js/theme/global.js`

Use es6 modules

* * *
##**Hooks**
Hooks are stores in the .hooks directory. So far, the only hook we have is the `pre-commit` hook that will run tests locally before committing your code. To install, make sure you're in the root of the project directory and run:
```
cp .hooks/pre-commit .git/hooks
```
Do not do this if you don't have all the dev dependencies installed. If you want to read up more on git hooks, go [here](https://www.atlassian.com/git/tutorials/git-hooks/local-hooks).
* * *

##**Debugging:**
Add `?debug=context` to your url.

note: do not depend on this for any of your pages, this will fail once pushed to production, debug only works on the local stencil servers
* * *
##**Testing:**
Testing is done using [Jasmine](https://github.com/jasmine/jasmine), and [Karma](https://karma-runner.github.io/0.13/index.html) as a test runner, and [PhantomJS](http://phantomjs.org/) as a headless browser. 

```
npm test
```

will run the tests
* * *
#####Useful documentation:
[Guide to git](http://rogerdudler.github.io/git-guide/)

[Stencil docs, especially this section](https://stencil.bigcommerce.com/docs/stencil-object-model)

Bootstrap [CSS](http://getbootstrap.com/css/) and [JS](http://getbootstrap.com/javascript/)

[Handlebars Syntax](http://handlebarsjs.com/)

[SCSS Syntax](http://sass-lang.com/guide)

[ES6 cheatsheet](https://github.com/DrkSephy/es6-cheatsheet)
# My project's README
